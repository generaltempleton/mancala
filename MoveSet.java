
public class MoveSet {

	private int[] moves = new int[100];//The combination of moves made on a single turn.
	private int[] bestMoves = new int[100];//The moves responsible for the best score.
	//Multiple moves implies multiple resets at home. It should never reach 100 but I will
	//include code to notify me if it somehow does so I can increase it.
	//a move is set to -1 if there is no move determined for that space. This won't be changed to 0 as that makes things complicated
	private int player;//Which player made that move
	private MoveSet parent;//The parent MoveSet (the one which goes before this one)
	private boolean functioning;//Whether or not the MoveSet attempts to move in a space with 0 stones.
	private boolean exhausted;//whether or not all the possible turns of the MoveSet have been tried
	private boolean stasis;//MoveSets are put into stasis once all their moves have been tried so that their best option is preserved.
	private int score;//The score this moveset allows
	private int bestScore;//The best score this element in the MSC can allow.
	
	public void increaseMove(){
		
		
		
		for(int i = moves.length-1; i>=0; i--){
			
			//System.out.println("###########");
			
			if(moves[i] != -1){
				
				System.out.println("###########");
				moves[i] += 1;//Increment the last one which is actually used
				System.out.println("A: " + moves[i]);
				carryOver();//Carry over
				break;//Don't keep hunting for another -1, only the first -1 is relevant and there are many more -1s.
			}
			
		}
		
	}
	
	public void scoreReport(int reportedScore){//Reports its score to the above MoveSet so as to tell it if it's effective
		
		score = 36 - reportedScore;
		//System.out.println("Best score" + score);
		//printBestMoves();
		
		if(score>bestScore && functioning == true){//can't accept a score from a MoveSet with an illegal move
			
			updateBest();
			
		}
		
	}
	
	public void updateBest(){
		//System.out.println("New best " + player + " " + score);
		bestScore = score;
		
		for(int i = 0; i<bestMoves.length; i++){
			
			bestMoves[i] = moves[i];
			
		}
		
	}
	
	public void carryOver(){
		
		int startPosition = 1;
		
		for(int i = 0; i<moves.length; i++){
			if(moves[i]==-1){
				startPosition = i-1;//Start at the place where the first actual move is being made.
				break;
			}
			
		}
		
		for(int i = startPosition; i>0; i--){//Start at 1 since you don't want to try to mess with moves[-1]
			
			if(moves[i]>5){
				moves[i-1]++;
				resetMoves(i, moves.length);//Set all moves past that point to -1
				
			}
			
		}
		
		if(moves[0]>5){//If you've used up all the possible options
			exhausted = true;
			scoreReport(bestScore);//Report the score to the parent now that all options have been exhausted.
		}
		
	}
	
	public void resetMoves(int start, int end){//This goes one short of end, so as to avoid trying to modify moves[moves.length]
		
		for(int i = start; i<end; i++){
			
			moves[i] = -1;
			
		}
	}
	
	public void printMoves(){
		for(int i = 0; moves[i]!=-1 && i<moves.length-1; i++){
			System.out.print(moves[i] + ", ");
		}
		
		System.out.println();//Give an extra line.
	}
	
	public int getMove(int depth){//Returns a move in the chain of moves made on one turn.
		return moves[depth];
		
	}
	
	public int getMoveBySpace(int depth){//Returns the space indicated by the move at the corresponding depth
		
		return moves[depth] + 7*player;
	}
	
	public int getPlayer(){//Returns which player made that move
		
		return player;
	}
	
	public boolean getStasis(){
		return stasis;
	}
	
	public boolean getExhausted(){
		return exhausted;
	}
	
	public void setMove(int depth, int value){//This one you input a 0-5 value as the move
		
		moves[depth] = value;
		//System.out.println("V: " + value);
		
	}
	
	
	public void setMoveBySpace(int depth, int value){//In this one you input a space and this is converted into a move and set.
		
		moves[depth] = value - 7 * player;
	}
	
	public void setFunctioning(boolean a){
		functioning = a;
	}
	
	
	public MoveSet(int playerInput){//Used for setting up the first MoveSet, which naturally has no
		//parent and should be player 0. Technically these two constructors could be merged.
		parent = null;
		functioning = true;
		exhausted = false;
		stasis = false;
		
		for( int i = 0; i<moves.length; i++){
			moves[i]=-1;
		}
		
		moves[0] = 0;
		
		player = playerInput;
		
		
		
		
	}
	
	public MoveSet(MoveSet myParent){//The constructor used for linking chains of MoveSets
		parent = myParent;

		functioning = true;
		exhausted = false;
		stasis = false;
		
		for( int i = 0; i<moves.length; i++){
			moves[i] = -1;
		}
		
		moves[0] = 0;//Set this one to 0 so as to avoid attempts to increment moves[-1]
		
		player = (parent.getPlayer()+1)%2;//The opposite of the one who moved before.
	}
}
