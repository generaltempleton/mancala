
public class MoveSetChain {

	private MoveSet[] chain = new MoveSet[100];
	
	public void increaseChain(){//This is like the increaseMove function but for a chain, increasing the last element
		// And carrying over.
		
		/*for(int i = 1; i<chain.length; i++){
			if(chain[i]==null){
				System.out.println("B: " + (i-1));
				chain[i-1].increaseMove();
				carryChain();
				break;//Don't keep messing around
			}
		}*/
		System.out.println("B: " + (findEndOfValid()-1));
		chain[findEndOfValid()-1].increaseMove();
		carryChain();
		
	}
	
	public void carryChain(){
		
		int startPosition = 0;
		
		for(int i = 1; i<chain.length; i++ ){
			if(chain[i]==null){
				startPosition = i-1;//Start off right before the ones.
				break;//Don't go further
			}
		}
		
		for(int i = startPosition; i > 0; i--){
			if(chain[i].getExhausted()==true){
				
				resetChain(i);//Reset the moves after the one that is exhausted.
				chain[i-1].increaseMove();
				
			}
		}
	}
	
	public void addMoveSet(){//Adds a new MoveSet to the chain
		
		for(int i = 0; i < chain.length; i++){
			
			if(chain[i]==null){
				chain[i] = new MoveSet(chain[i-1]);
				break;//GODDAMMIT! So much hardship from this.
			}
		}
		
	}
	
	public int findEndOfValid(){
		
		for(int i = 0; i<chain.length; i++){
			if(chain[i]==null){
				return i-1;
			}
		}
		return chain.length - 1;
	}
	
	public void resetChain( int resetPoint ){
		
		for(int i = resetPoint; i<chain.length; i++){
			chain[i] = null;//Now these are set to null so that they can be recreated only when needed.
		}
		
	}
	
	public void printMoveSetChain(){
		int end = findEndOfValid();
		for(int i = 0;  i < end; i++){
			
			System.out.print(i + ": ");//Formats in a 0: 1, 5, 5
			chain[i].printMoves();
		}
	}
	
	public void printMoveSetChain(int start, int end){
		if(end>start){
			for(int i = start; i<=end; i++){
				System.out.print(i + ": ");
				chain[i].printMoves();
			}
		}

	}
	
	public MoveSet getMoveSet(int depth){
		
		while(chain[depth]==null){
			addMoveSet();
		}
		
		return chain[depth];
		
	}
	
	public MoveSetChain(int firstPlayer){//This function's input specifies which player goes first
		//In all likelihood, it will remain 0 the entire time as player 0 should go first
		for(int i = 0; i<chain.length; i++){
			chain[i] = null;
		}
		chain[0] = new MoveSet(firstPlayer);
		

		//Let all the other ones default to null as they haven't proven themselves necessary.
	}
}
