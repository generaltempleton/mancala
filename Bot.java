
public class Bot {
	
	
	public static void main(String[] args){
		
		MoveSetChain playerMoves = new MoveSetChain(0);
		playerMoves.printMoveSetChain();
		
		Board myBoard = new Board(3, playerMoves);
		
		while(playerMoves.getMoveSet(0).getExhausted()==false){
			
			myBoard.playGame();
			
			playerMoves.increaseChain();
			playerMoves.printMoveSetChain();
		}
		
	}

}
