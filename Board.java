
public class Board {

	
	private int boardStoneCount;//The number of stones in each square of the board at the start
	private int moveDepth;//How far through the MSC you are currently
	private int[] spaces = new int[14];//6 normal holes on each side + 2 home spaces.
	private boolean gameOver;
	private MoveSetChain associatedMSC;
	
	public void playGame(){
		
		resetBoard();
		
		while(gameOver == false){
			
			
			if(associatedMSC.getMoveSet(moveDepth)==null){
				associatedMSC.addMoveSet();
			}
			
			turn(associatedMSC.getMoveSet(moveDepth));
			//printBoard();
			moveDepth++;
			//System.out.println(moveDepth);
		}
		
		//System.out.println("Endgame");
		
		
	}

	public boolean move(MoveSet a, int depth){
		
		if(depth>400){
			System.out.println("Depth >400!");
		}
		
		
		if(a.getMove(depth)>5){//If it's not a legal move
			gameOver = true;//End the game
			a.setFunctioning(false);//Invalidate the data
			System.out.println(">5 move attempted!");//Display warning message.
			return false;//Don't allow the player to go again.
		}
		
		if(a.getMove(depth)==-1){
			a.setMove(depth, 0);//Set it to 0 so that you don't try to pick up from spaces[-1].
		}
		
		int hand;//The number of stones in your hand at a given time
		int position = a.getMoveBySpace(depth);//This way it starts at the position given by the player's perspective and their choice
		int player = a.getPlayer();
		
		
		
		if(spaces[position]==0 && a.getStasis()==false){//If there isn't a stone there, then change the move so that it does pick one up
			//This is done before the stones are picked up so that the pickup can be successful.
			
			a.setMoveBySpace( depth, findNextFilledSpace(position, player) );
			//System.out.println(depth);
			//a.setMove(depth, a.getMove(depth)+1);
			a.carryOver();

		}
		
		position = a.getMoveBySpace(depth);//Set it to the NEW position so that it doesn't fail trying something old.
		hand = spaces[position];
		spaces[position] = 0;
		
		if(hand==0){
			a.setFunctioning(false);//This shouldn't be able to happen since there should always be a stone on your side of the board
			gameOver = true;
			a.setMove(depth, -1);
			return false;

			//While the game is running.
		}
		
		while(hand>0){
			
			position++;//move 1 forward
			
			if((player==0 && position == 13) || (player==1 && position == 6)){
			//you can't put stones in the opponent's goal
				
				position++;//move to the next space, that way you don't try to drop stones in the opponent's goal;
				
				
			}
			
			position %= 14;//This circles back around to avoid making illegal moves
			
			hand--;//Take the stone out of your hand
			spaces[position] += 1;//Add that stone to your current space
			
			//cleanHouse();//This is experimental and MAY NEED TO BE MOVED!
			
			
			if(hand == 0){
				if(position != 6 && position != 13 && spaces[position]>1){//If you aren't in a home square
					//and landed on a space with stones, pick up the stones
					//printBoard();
					//System.out.println(position);
					//System.out.println("Pickup " + spaces[position] + " from " + position);
					hand = spaces[position];//Pick up stones
					spaces[position] = 0; // Remove the stones from that space
				} else if(position==6 || position == 13){//You landed in your home square
					//System.out.println("Go Again!");
					cleanHouse();
					return true;
					} else{
					
					//System.out.println("False Reported");
					cleanHouse();
					return false;//You don't get to go again.

				}
				
				
			}
			
		}	
		System.out.println("ERROR A1");
		cleanHouse();
		return false;//The code should never reach this point because the if else if else series should handle all cases. 
		//Thus it is an error if this is reached.
		
	}

	public void turn(MoveSet b){//plays out a full turn using a full MoveSet
		int depth = 0;//How many moves you end up making in one turn.
		//moveDepth = 0;
				
		while(move(b, depth) == true && gameOver == false){
			
			depth++;
			//System.out.println("Depth: " + depth);
			
			//cleanHouse();
		}
		

	}
	
	public void cleanHouse(){//If all rows on one side are empty at the end of a move,
		//then all the stones on both sides get added to their respective homes
		
		int p0Sum = 0;
		int p1Sum = 0;
		

		

		for(int i = 0; i<6; i++){//Add up the number of stones on player 0's side
			p0Sum += spaces[i];
			}
		
		for(int i = 7; i<13; i++){//Add up the number of stones on player 1's side
			p1Sum += spaces[i];
			}
		
	
		if(p0Sum == 0 || p1Sum == 0){
			
			gameOver = true;
			System.out.println("Over.");
			//System.out.println(moveDepth);
			
			spaces[6]+=p0Sum;//Add the total to the respective sides
			spaces[13]+=p1Sum;
			
			
			System.out.println( spaces[6] + ":" + spaces[13]);

			for(int i = 0; i<6; i++){//Clear out the board (probably not necessary)
				spaces[i] = 0;
				}
			
			for(int i = 7; i<13; i++){//Clear out the board (probably not necessary)
				spaces[i] = 0;
				}
			
			associatedMSC.getMoveSet(moveDepth).scoreReport(moveDepth%2==0 ? p0Sum : p1Sum);
		
			}
		
	}
	
	public int findNextFilledSpace(int startingSpace, int player){
		
		for(int i = startingSpace; i<6+7*player; i++){//look in all the spaces on that player's side
			
			if(spaces[i]>0){//If you find one that does have a stone in it
				System.out.println("Set to: " + i);
				return i;//Set the moveset to use that one instead.
				
			}
			
		}
		
		return 5+7*player;//Returns the last space so the move gets reset afterwards. There may be a more efficient
		//Way to tell the MoveSet that there are no more valid options. This may be added later. 
	}
	
	public void resetBoard(){//This starts the game from the beginning, resetting the necessary aspects of the board.
		
		for(int i = 0; i < 14; i++){
			spaces[i] = boardStoneCount;
		}
		gameOver = false;//The game never ends before starting.
		moveDepth = 0; // You have to start with the first player's first move.
		spaces[6] = 0; // players start out with no points
		spaces[13] = 0;
		
	}
	
	public void printBoard(){
		
		for(int a : spaces){
			System.out.print(a + " ");
		}
		
		System.out.println();
	}

	
	
	public Board(int stoneCount, MoveSetChain MSC){//constructor to create the board
		//MSC stands for MoveSetChain
		
		associatedMSC = MSC;
		gameOver = false;//The game hasn't yet been ended
		moveDepth = 0;//You start out by playing the first player's first turn
		
		boardStoneCount = stoneCount;
		
		for(int i = 0; i < 14; i++){
			spaces[i] = stoneCount;
		}
		
		spaces[6] = 0; // players start out with no points
		spaces[13] = 0;
		
		//printBoard();
	}


}
